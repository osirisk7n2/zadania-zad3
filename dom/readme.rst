
Praca domowa #3
===============

Stworzyć bardziej złożony mashup dla tych danych, co Państwa interesują.
Przykłady:
 * mapa rozmieszczenia browarów lub pubów w pobliżu domu
 * wielo-osiowy wykres popularności różnych miast w kilku kategoriach
 * wizualizacja głosowań danego polityka lub partii (np. http://mamprawowiedziec.pl/glosowania)

Masz zainteresowania, Sieć ma narzędzia — połącz je, by stworzyć coś nowego.