# -*- encoding: utf-8 -*-

import requests
import urllib
import string
import webbrowser
# import json



print "Witamy w GeoEvent MASHUP. Nasza aplikacja umożliwia Ci wyszukanie imprez, koncertów w Twoim najbliższym otoczeniu."
while True:
    req_address = raw_input('Podaj miasto albo dokładny adres, w którego okolicy chcesz znaleźć eventy\r\n(Niestety API, z którego korzystamy ma zindeksowane tylko większe miasta typu: Warszawa, Londyn, Nowy Jork): ')
    # req_address = 'Nowy jork'
    req_base = 'http://maps.googleapis.com/maps/api/geocode/json'

    req_data = {'address': req_address, 'sensor': 'false' }
    response = requests.get(req_base, params=req_data)


    odp = response.json()
    if odp['status'] == 'OK':
        if odp['results'][0]['geometry']['location_type'] == 'ROOFTOP':
            location = odp['results'][0]['address_components'][3]['long_name']
        elif odp['results'][0]['geometry']['location_type'] == 'APPROXIMATE':
            location = odp['results'][0]['address_components'][0]['long_name']
        else:
            location = odp['results'][0]['address_components'][1]['long_name']

        # location =raw_input("Podaj nazwe miasta (po angielsku): ")
        #api bandsintown zwracajace informacje o wszelkiego rodzaju eventach w np danym miescie (dla potrzeb tej aplikacji)
        base ='http://api.bandsintown.com/events/search'
        data={"location":location,"radius":"5","format":"json"}
        request = requests.get(base, params=data)
        querry="size=2024x2600&maptype=roadmap"
        data = request.json()
        tmp=""
        if len(data) > 2:
            #licznik i litery potrzebne do oznaczenia markerow googlowskich
            licznik=0
            letters =string.ascii_uppercase
            latlong = ""
            htmlMarkers="<p><img src=\"file.png\" alt=\"mapa\"</img></p><p><h2>Zaznaczone Eventy:</h2></p>"
            htmlRest="<p><h2>Reszta Eventow</h2></p>"
            for event in data:
                # print event
                #google ogranicza ilosc markerow jakie mozemy umiescic na mapie (jest to wymuszone dlugoscia linka
                #stad tez ten licznik, pozostaly warunek sprawdza aby nie nakladac na siebie markerow bo ostatecznie wtedy i tak
                #nie bylo by ich widac
                if(((str(round(event['venue']['latitude'],3))+str(round(event['venue']['longitude'],3))) not in latlong)and licznik<15):
                    tmp+="&markers=color:red|"+"label:"+letters[licznik]+"|"+str(round(event['venue']['latitude'],3))+","+str(round(event['venue']['longitude'],3))
                    date_arr = event['datetime'].split('T')
                    date = date_arr[0] + " " + date_arr[1]
                    #generujemyy odpowiedniego HTMLa aby potem to wszystko zgrabnie wyswietlic z opisem
                    htmlMarkers+="<dl><dt><b>"+letters[licznik]+" - </b> "+event['venue']['name']+" "+date+"</dt>"
                    for artist in event['artists']:
                        htmlMarkers+="<dd><a href=\""+artist['url']+"\">"+artist['name']+"</a>"+"</dd>"
                    htmlMarkers+="</dl>"
                    latlong=latlong+str(round(event['venue']['latitude'],3))+str(round(event['venue']['longitude'],3))
                    licznik=licznik+1
                else:
                    #else czyli jesli koordynaty juz znajduja sie na mapie lub licznik >16 to generujemy tylko htmla bez markerow
                    htmlRest+="<dl><dt> "+event['venue']['name']+" "+date+"</dt>"
                    for artist in event['artists']:
                        htmlRest+="<dd><b>~</b><a href=\""+artist['url']+"\">"+artist['name']+"</a>"+"</dd>"
                    htmlRest+="</dl>"
                #print htmlMarkers+htmlRest


            #odwolanie do googlowskiego API static maps
            googleMapsBase='http://maps.googleapis.com/maps/api/staticmap'
            rq=requests.get(googleMapsBase,params=querry+tmp+"&sensor=false")

            #odbieramy obrazek z google api (moglibysmy umiescic bezposredni link ale chyba lapiej miec to na dysku...
            urllib.urlretrieve(rq.url,"file.png")
            f=open("plik.html","w")
            #zaposujemy obrazek i gotowa strone jako html
            f.write(str(htmlMarkers.encode("utf-8")+htmlRest.encode("utf-8")))
            #wywolujemy funkcje otwierajaca nam przegladarke i jakos wartosc wstawiamy uprzedmio zapisany html
            webbrowser.open_new("plik.html")

            print "1. Zamknij aplikację"
            print "2. Sprawdź inny adres"

            choice = raw_input()
            if choice == '1':
                break
            elif choice == '2':
                continue
            else:
                break
        else:
            print "Nie znaleziono eventów w podanej lokalizacji, bądź nie zdefiniowałeś miasta"

    else:
        print("Nie znaleziono podanej lokalizacji, proszę spróbować ponownie")
